import { Component, OnInit } from '@angular/core';
import { PrestadoresAccesoTercerosService } from '../../servicios/prestadores-acceso-terceros.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-validador-bonos',
  templateUrl: './validador-bonos.component.html',
  styleUrls: ['./validador-bonos.component.css']
})
export class ValidadorBonosComponent implements OnInit {
  bono = null;
  nro_orden : number; //prueba: 648059

  constructor(private SrvPAT: PrestadoresAccesoTercerosService, private SrvToastr: ToastrService) { 
    
  }


  ngOnInit() {
  }

  verificarUsoBono() {
    if(this.nro_orden!=undefined) {
      this.SrvPAT.verificarUsoBono(this.nro_orden).subscribe(resp => {
        this.bono = resp[0];
        console.log(this.bono);
      }, err => {
        console.error(err);
      });
    } else {
      this.SrvToastr.info("Debe ingresar un número de orden", "Corregir");
    }
  
  }

  volver() {
    this.bono=null;
    this.nro_orden=null;
  }

  guardarUsoBono() {
    if(this.nro_orden!=undefined) {
      this.SrvPAT.insertUsoOrdenesServicio(this.nro_orden).subscribe(() => {
          this.SrvToastr.success("El uso de la orden se ha confirmado exitosamente.", "Exito!");
        this.bono=null;
      }, error => {
        console.error(error);
          this.SrvToastr.error("Ha ocurrido un error al marcar el uso de la orden...", "ERROR");
      })
    }
  }

}
