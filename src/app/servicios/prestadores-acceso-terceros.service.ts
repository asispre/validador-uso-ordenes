import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { environment} from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class PrestadoresAccesoTercerosService {

  constructor(private http: HttpClient) { }

  verificarUsoBono(orden){
    const httpOptions = {
      headers: new HttpHeaders(
        { 'Content-Type': 'application/json',
          'Authorization':  environment.apiKey
        }
      )
    };
    return this.http.get(`${environment.apiEndpoint}/asispre/prestadores/verificarUsoBono/${orden}/`, httpOptions);
  }

  
  insertUsoOrdenesServicio(orden: any) {
    const httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': environment.apiKey
        }
      )
    };
    
    const json = JSON.stringify({"orden" : orden});
    return this.http.post(`${environment.apiEndpoint}/asispre/prestadores/insertUsoOrdenesServicio/`, json, httpOptions);
  }
  
}
